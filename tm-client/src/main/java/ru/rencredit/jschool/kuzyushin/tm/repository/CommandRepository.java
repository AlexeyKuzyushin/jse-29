package ru.rencredit.jschool.kuzyushin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ICommandRepository;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> commandList = new ArrayList<>();

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections =
                new Reflections("ru.rencredit.jschool.kuzyushin.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            commandList.add(clazz.newInstance());
        }
    }

    {
        initCommands();
    }

    @Override
    public @NotNull List<AbstractCommand> getCommandList() {
        return commandList;
    }
}
