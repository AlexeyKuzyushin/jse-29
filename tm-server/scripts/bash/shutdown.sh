#!/bin/bash

echo '===> Shutdown tm-server'
echo ''
if [ ! -f tm-server.pid ]; then
	echo "tm-server pid not found"
	exit 1;
fi

echo 'tm-server was stopped';
kill -9 $(cat tm-server.pid)
rm tm-server.pid
