package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.ITaskEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
@NoArgsConstructor
public final class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    public TaskEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public Long countAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getTaskService().count();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session")  final @Nullable SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getTaskService().findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByUserId(
            @WebParam(name = "session", partName = "session")  final @Nullable SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findAll(sessionDTO.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByProjectId(
            @WebParam(name = "session", partName = "session")  final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId")  final @Nullable String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getTaskService().findAll(sessionDTO.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getTaskService().clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createTask(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId")  final @Nullable String projectId,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().create(sessionDTO.getUserId(), projectId, name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable String id) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return TaskDTO.toDTO(serviceLocator.getTaskService().findById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public TaskDTO findTaskByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return TaskDTO.toDTO(serviceLocator.getTaskService().findByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeAllTasksByUserId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTasksByProjectId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId")  final @Nullable String projectId) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().removeAllByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description) {
        return serviceLocator.getTaskService().updateById(sessionDTO.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskStartDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().updateFinishDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateTaskFinishDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") final @Nullable Date date) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getTaskService().updateFinishDate(sessionDTO.getUserId(), id, date);
    }
}
