package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;

public interface IUserEndpoint {

    @NotNull
    void updateUserLogin(@Nullable SessionDTO sessionDTO, @Nullable String login);

    @NotNull
    void updateUserPassword(@Nullable SessionDTO sessionDTO, @Nullable String password);

    @Nullable
    void updateUserEmail(@Nullable SessionDTO sessionDTO, @Nullable String email);

    @Nullable
    void updateUserFirstName(@Nullable SessionDTO sessionDTO, @Nullable String firstName);

    @Nullable
    void updateUserLastName(@Nullable SessionDTO sessionDTO, @Nullable String lastName);

    @Nullable
    void updateUserMiddleName(@Nullable SessionDTO sessionDTO, @Nullable String middleName);

    @Nullable
    UserDTO viewUserProfile(@Nullable SessionDTO sessionDTO);
}
