package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;

import java.util.List;

public interface IService <T extends AbstractEntity> {

    void remove (@NotNull T t);

    void persist(@Nullable T t);

    void merge(@Nullable T t);
}
